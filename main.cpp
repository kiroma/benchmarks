#include <iostream>
#include <random>
#include <chrono>
#include <thread>
#include <future>

bool something(bool& b)
{
	return b;
}

int main()
{
	bool d = true;
	auto a = std::chrono::high_resolution_clock::now();
	std::thread t(something, std::ref(d));
	auto b = std::chrono::high_resolution_clock::now();
	t.join();
	std::chrono::duration<long double, std::nano> dur = b-a;
	a = std::chrono::high_resolution_clock::now();
	std::cout << dur.count() << "\n";
	b = std::chrono::high_resolution_clock::now();
	dur = b-a;
	std::cout << dur.count() << std::endl;
	getchar();
	return 0;
}
